provider "aws" {
}

resource "aws_vpc" "main" {
  cidr_block = "10.1.0.0/16"
  tags = {
    name = "Terraform Example VPC"
  }
}

resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id
}

resource "aws_subnet" "main" {
  vpc_id = aws_vpc.main.id
  cidr_block = "10.1.1.0/24"
}

resource "aws_route_table" "default" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main.id
  }
}

resource "aws_route_table_association" "main" {
  subnet_id = aws_subnet.main.id
  route_table_id = aws_route_table.default.id
}

resource "aws_network_acl" "allowall" {
  vpc_id = aws_vpc.main.id

  egress {
    protocol = "-1"
    rule_no = 100
    action = "allow"
    cidr_block = "0.0.0.0/0"
    from_port = 0
    to_port = 0
  }

  ingress {
    protocol = "-1"
    rule_no = 200
    action = "allow"
    cidr_block = "0.0.0.0/0"
    from_port = 0
    to_port = 0
  }
}

resource "aws_security_group" "allowall" {
  name = "Terraform AWS Example Allow All"
  description = "Allows all traffic - naughty"
  vpc_id = aws_vpc.main.id

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_eip" "webserver" {
  instance = aws_instance.webserver.id
  vpc = true
  depends_on = ["aws_internet_gateway.main"]
}

resource "aws_key_pair" "default" {
  key_name = "terraform_aws_example_ssh_key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDdGLMWyrqxQBBeovUo5QdVSc0DB+176eH/fbPhe6U3WoKEkkGJ/wc4KR+dW8mMbD7r9jRcGAZbsp2Mc8C2fKK5k71/0kHp4K5LqeQFCu6K7qHdoggpJyHjGOUtgCXab//tmhiatntUqQMt6r4TqlBEhi5zWfVS4FStYcsrSYi/SLzUbyPuhGko7DdoLdsvHBT1nlJwLIwjh5hmHABDsG4MkYT/0XLpCQcuPy3EsHWs65XX2Pr3Qk9tqDh/FG6frWDuosOXQlXH0IjxfAsrZhOFKYy96p+DqHuuCr9OE8Ic2iI+QaY8vOJtNTl8iwfGoaXfKQO3RnADlKdrwyvwLf9B sheska@jain.local"
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "webserver" {
  ami = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  key_name = "terraform_aws_example_ssh_key"
  vpc_security_group_ids = [aws_security_group.allowall.id]
  subnet_id = aws_subnet.main.id
}

output "name" {
  value = aws_eip.webserver.public_ip
}
 
